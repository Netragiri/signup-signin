import React from 'react'
import { Navigate, useLocation,useNavigate } from 'react-router-dom'
import {LogInAuth} from './LogInAuth';

function ProtectedAuth({children}) {
  const auth=LogInAuth()
  
if(!auth.user){
  return  <Navigate to='/login'
 />
}
  return (
   children
  )
}

export default ProtectedAuth
