import './App.css';
import SignUp from './Screens/SignUp';
import SignIn from './Screens/SignIn';
import NavigationBar from './Components/NavigationBar';
import HomeRouters from './HomeRouters';
import { AuthProvider } from './Utills/LogInAuth';



function App() {
  return (
    <div className="App">
      <AuthProvider>
        <NavigationBar />
        <HomeRouters />
      </AuthProvider>
    </div>
  );
}

export default App;
