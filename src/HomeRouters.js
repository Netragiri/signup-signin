import React from 'react'
import{Routes,Route} from 'react-router-dom'
import Home from './Components/Home'
import { AuthProvider } from './Utills/LogInAuth'
import ProtectedAuth from './Utills/ProtectedAuth'
import DashBoard from './ProtectedScreen/DashBoard'
import SignIn from './Screens/SignIn'
import SignUp from './Screens/SignUp'


function HomeRouters() {
  return (
    <div>
      
      <Routes>
          <Route path='' element={<Home />}></Route>
          <Route path='dashboard' element={
            <ProtectedAuth>
              <DashBoard />
            </ProtectedAuth>
          }></Route>
          <Route path='signup' element={<SignUp />}></Route>
          <Route path='login' element={<SignIn />}></Route>
      </Routes>
     
    </div>
  )
}

export default HomeRouters
