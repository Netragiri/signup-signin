import React from 'react'
import {Link} from 'react-router-dom'
import { LogInAuth } from '../Utills/LogInAuth'


function NavigationBar() {
  const auth=LogInAuth()
  const handlelogout = () =>{
    auth.logout();
  }
  return (
    <div>
     < nav className="navbar navbar-expand-lg navbar-light bg-light">
  <Link className="navbar-brand" to="">Welcome</Link>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav nav">
      <li className="nav-item">
        <Link className="nav-link" to="dashboard">Dashboard</Link>
      </li>
    </ul>
    {!auth.user ? <><ul className='nav navbar-right'>
    <li className="nav-item">
        <Link className="nav-link" to="signup">Sign Up</Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to="login">Log In!!!!</Link>
      </li>
    </ul></> : <ul className='nav navbar-right'><li className='nav-item'><Link to='/login'>  <a onClick={handlelogout}>Logout</a></Link></li></ul> }
    
      
  </div>
</nav>
    </div>
  )
}

export default NavigationBar
