import { Formik,Form,Field,ErrorMessage } from 'formik'
import React from 'react'
import { useNavigate } from 'react-router-dom';
import * as yup from 'yup';
import {LogInAuth} from '../Utills/LogInAuth';
import {Link} from'react-router-dom'


//first get data from the localstorage 

function SignIn() {
  const auth = LogInAuth()
  const navigate=useNavigate()
  
  //validation for 
  // const validation=yup.object().shape({
    //     username:yup.string().
    //             matches(`^${data.username}$`,'data not exist').
    //             // min(data.username.length,'data not matches').
    //             // max(data.username.length,'data not matches').
    //             required(),
    //     password:yup.string().
    //             matches(`^${data.password}$`,'password does not matche').
    //             // min(data.password.length,'').
    //             // max(data.username.password,'').
    //             required()
    // })
    
    
    
    //for printing the error message
    const Error=({name})=>{
      return(
        <>
          <div style={{color:'red'}}>
            <ErrorMessage name={name} />
            </div>
            </>
        )
      }
      
      //for to check the correct autorise user then navigate the user to the dashboard
      const handlesubmit = (values)=>{
        const data=JSON.parse(atob(localStorage.getItem('data')));
        console.log(values)
        if((data.username==values.username) && (data.password==values.password)){
          auth.login(values.username)
          navigate('/dashboard')
        }
        else{
          alert('invalid username or password')
        }
        
      }
      return (
    <div>
        <h1>Log In</h1>
      <Formik
      //  validationSchema={validation}
               initialValues={{
                                username:'',
                                password:''
                            }}
               onSubmit={handlesubmit}>


          <div className='container my-5'>
                <Form>
                    <strong>Username:</strong>
                    <Field type='text' name='username' placeholder='enter your name' className='form-control' />
                    {/* <Error name='username' /> */}


                    <strong>Password:</strong>
                    <Field type='password' name='password' placeholder='enter your password' className='form-control' />
                    {/* <Error name='password' /> */}
                    
                    <button type='submit' className='my-5'>Log in</button>

                    <p>Don't have an account</p>
                    <Link to='/signup'>Sign Up</Link>
                </Form>
          </div>
      </Formik>
    </div>
  )
}

export default SignIn
