import { Formik, Form, Field, ErrorMessage } from 'formik'
import React from 'react'
import { Navigate } from 'react-router-dom';
import * as yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { initialValues, validation } from '../Utills/ValidationSignUp';

function SignUp() {

  //for navifgatinf one page to another
  const navigate = useNavigate();


  //to print error message for validation
  const Error = ({ name }) => {
    return (
      <>
        <div style={{ color: 'red' }}>
          <ErrorMessage name={name} />
        </div>
      </>
    )
  }

  return (
    <div>
      <h1>Sign up</h1>
      <Formik validationSchema={validation} initialValues={initialValues}
        onSubmit={(item) => {

          //to encrypt data
          const encData = {username:btoa(item.username),email:btoa(item.email),password:btoa(item.password),confirm_passsword:btoa(item.confirm_password),number:btoa(item.number),city:btoa(item.city),state:btoa(item.state)}

          localStorage.setItem("data", btoa(JSON.stringify(item)))
          navigate('/login')
          console.log(item)
        }}>

        <div className='container' >

          <Form >

            <strong>Username:</strong>
            <Field type='text' placeholder='enter your name here' className='form-control' name='username' id='username' /><Error name='username' />

            <strong>Email-id:</strong>
            <Field type='text' placeholder='email-id' id='email' className='form-control' name='email' /><Error name='email' />

            <strong> Password:</strong>
            <Field type='password' placeholder='enter password' id='password' className='form-control' name='password' /><Error name='password' />

            <strong>Confirm_password:</strong>
            <Field type='password' placeholder='confirm password' id='confirm_password' className='form-control' name='confirm_password' /><Error name='confirm_password' />

            <strong> Phone_no:</strong>
            <Field type='text' placeholder='enter phone no.' id='number' className='form-control' name='number' /><Error name='number' />


            <strong>City:</strong>
            <Field as='select' className="form-control form-control-sm" name='city'>
              <option value=''>select</option>
              <option value='gujrat'>Gujrat</option>
              <option value='RAJASTHAN'>Rajsthan</option>
              <option value='keral'>Keral</option>
              <option value='others'>Others</option>
            </Field><Error name='city' />

            <strong> State:</strong>
            <Field as='select' className="form-control form-control-sm" name='state'>
              <option>Select</option>
              <option>Ahmedabad</option>
              <option>Jaipur</option>
              <option>Chennai</option>
              <option>Other</option>
            </Field><Error name='state' />




            <button type='submit' className='my-5'>Submit</button>

          </Form>
        </div>

      </Formik>
    </div>
  )
}

export default SignUp
